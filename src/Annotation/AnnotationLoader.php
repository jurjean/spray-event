<?php

declare(strict_types = 1);

namespace Spray\Event\Annotation;

class AnnotationLoader
{
    public function __invoke()
    {
        require_once __DIR__ . '/AggregateId.php';
        require_once __DIR__ . '/AggregateRoot.php';
        require_once __DIR__ . '/EventSourcingHandler.php';
        require_once __DIR__ . '/CommandHandler.php';
    }
}