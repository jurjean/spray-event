<?php

declare(strict_types = 1);

namespace Spray\Event\EventHandling;

use Doctrine\Common\Annotations\AnnotationReader;
use RuntimeException;
use Spray\Event\Annotation;
use ReflectionClass;
use Spray\Event\EventHandling\Exception\AggregateNotIdentifiedException;

class AnnotatedAggregateEventHandler implements EventHandler
{
    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    /**
     * @var object
     */
    private $aggregate;

    /**
     * @var string
     */
    private $aggregateIdProperty;

    /**
     * @var array
     */
    private $handlers = array();

    /**
     * @var boolean
     */
    private $hasAggregateId;

    public function __construct(
        AnnotationReader $annotationReader,
        $aggregate)
    {
        $this->annotationReader = $annotationReader;
        $this->aggregate = $aggregate;

        $reflection = new ReflectionClass($aggregate);
        foreach ($reflection->getProperties() as $propertyReflection) {
            $annotations = $annotationReader->getPropertyAnnotations($propertyReflection);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Annotation\AggregateId) {
                    $this->aggregateIdProperty = $propertyReflection->getName();
                }
            }
            if ( ! $this->aggregateIdProperty) {
                throw new AggregateNotIdentifiedException(sprintf(
                    'Aggregate identifier property not found, please annotate with @AggregateId'
                ));
            }
        }
        foreach ($reflection->getMethods() as $methodReflection) {
            $annotations = $annotationReader->getMethodAnnotations($methodReflection);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Annotation\EventSourcingHandler) {
                    $methodName = $methodReflection->getName();
                    $this->handlers[$methodReflection->getParameters()[0]->getClass()->getName()] = function($command) use ($methodName) {
                        $this->$methodName($command);
                    };
                }
            }
        }
    }

    public static function factory($aggregate): EventHandler
    {
        return new AnnotatedAggregateEventHandler(
            new AnnotationReader(),
            $aggregate
        );
    }

    private function accepts($event)
    {
        return isset($this->handlers[get_class($event)]);
    }

    public function handle($event)
    {
        if ( ! $this->accepts($event)) {
            throw new RuntimeException(sprintf(
                'Event %s cannot be handled by this handler, are you sure you annotated it with @Handle?',
                get_class($event)
            ));
        }

        $this->handlers[get_class($event)]->call($this->aggregate, $event);

        if ( ! $this->hasAggregateId) {
            $check = \Closure::bind(function($aggregate, $property) {
                if ( ! $aggregate->$property) {
                    throw new AggregateNotIdentifiedException(sprintf(
                        'Aggregate has no identifier set after event'
                    ));
                }
            }, null, get_class($this->aggregate));
            $check($this->aggregate, $this->aggregateIdProperty);
            $this->hasAggregateId = true;
        }
    }
}
