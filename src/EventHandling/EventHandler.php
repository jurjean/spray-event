<?php

declare(strict_types = 1);

namespace Spray\Event\EventHandling;

interface EventHandler
{
    public function handle($event);
}