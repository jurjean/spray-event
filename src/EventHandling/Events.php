<?php

namespace Spray\Event\EventHandling;

interface Events
{
    /**
     * Inject events into the aggregate from history.
     *
     * @param ...$events
     *
     * @return void
     */
    public function inject(...$events);

    /**
     * Extract events from the aggregate that are not committed.
     *
     * @return array
     */
    public function extract(): array;

    /**
     * Apply an event to the aggregate.
     *
     * @param ...$events
     *
     * @return void
     */
    public function apply($event);
}
