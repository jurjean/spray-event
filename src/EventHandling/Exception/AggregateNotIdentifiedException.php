<?php

declare(strict_types = 1);

namespace Spray\Event\EventHandling\Exception;

use Spray\Event\Exception\RuntimeException;

class AggregateNotIdentifiedException extends RuntimeException implements Exception
{

}