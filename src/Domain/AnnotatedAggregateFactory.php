<?php

namespace Spray\Event\Domain;

use Closure;
use Spray\Event\EventHandling\AnnotatedAggregateEventHandler;

class AnnotatedAggregateFactory implements AggregateFactory
{
    public function build(string $aggregateClass)
    {
        $aggregate = $this->construct($aggregateClass);
        $events = new AggregateEvents(AnnotatedAggregateEventHandler::factory($aggregate), $aggregateIdProperty);
        $bind = Closure::bind(function($aggregate) use ($events) {
            $aggregate->events = $events;
        }, null, AnnotatedAggregateRoot::class);
        $bind($aggregate);
        return $aggregate;
    }

    private function construct(string $aggregateClass)
    {
        return unserialize(
            sprintf(
                'O:%d:"%s":0:{}',
                strlen($aggregateClass),
                $aggregateClass
            )
        );
    }
}