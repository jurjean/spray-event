<?php

namespace Spray\Event\Domain;

interface AggregateFactory
{
    public function build(string $aggregateClass);
}