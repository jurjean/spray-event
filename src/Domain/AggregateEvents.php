<?php

declare(strict_types=1);

namespace Spray\Event\Domain;

use Closure;
use RuntimeException;
use Spray\Event\EventHandling\EventHandler;
use Spray\Event\EventHandling\Events;

class AggregateEvents implements Events
{
    /**
     * @var EventHandler
     */
    private $eventHandler;

    /**
     * @var boolean
     */
    private $recording = true;

    /**
     * @var array
     */
    private $records = array();

    /**
     * @param EventHandler $eventHandler
     */
    final public function __construct(EventHandler $eventHandler)
    {
        $this->eventHandler = $eventHandler;
    }

    /**
     * Inject events into the aggregate from history.
     *
     * @param ...$events
     *
     * @return void
     */
    public function inject(...$events)
    {
        $this->recording = false;
        foreach ($events as $event) {
            $this->apply($event);
        }
        $this->recording = true;
    }

    /**
     * Extract events from the aggregate that are not committed.
     *
     * @return array
     */
    public function extract(): array
    {
        return $this->records;
    }


    /**
     * @param $event
     */
    public function apply($event)
    {
        if ($event instanceof Closure) {
            return $this->applyCallable($event);
        } else if (is_object($event)) {
            return $this->applyEvent($event);
        }
        throw new RuntimeException(sprintf(
            'Can not apply event of type %s', gettype($event)
        ));
    }

    /**
     * @param Closure $callable
     */
    protected function applyCallable(Closure $callable)
    {
        foreach ($callable as $event) {
            $this->apply($event);
        }
    }

    /**
     * @param object $event
     */
    protected function applyEvent($event)
    {
        $this->eventHandler->handle($event);

        if ($this->recording) {
            $this->records[] = $event;
        }
    }
}
