<?php

namespace Spray\Event\Domain;

use Closure;
use Spray\Event\EventHandling\Events;

abstract class AnnotatedAggregateRoot implements AggregateRoot
{
    /**
     * @var Events
     */
    private $events;

    /**
     * No constructor is called.
     */
    final private function __construct()
    {

    }

    /**
     * @return Events
     */
    final protected function apply(...$events)
    {
        $this->events->apply(...$events);
    }
}
