<?php

namespace Spray\Event\Domain;

interface Repository
{
    public function load(string $aggregateId): AggregateRoot;

    public function save(string $aggregateId, AggregateRoot $aggregate);
}