<?php

declare(strict_types = 1);

namespace Spray\Event\Domain\Exception;

use Spray\Event\Exception\RuntimeException;

class AggregateNotFoundException extends RuntimeException implements Exception
{

}