<?php

declare(strict_types=1);

namespace Spray\Event\Domain;

use Spray\Event\Store\Store;

class AggregateRepository implements Repository
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var AggregateFactory
     */
    private $factory;

    /**
     * @var string
     */
    private $aggregateType;

    /**
     * @param Store $store
     * @param AggregateFactory $factory
     * @param string $aggregateType
     */
    public function __construct(Store $store, AggregateFactory $factory, string $aggregateType)
    {
        $this->store = $store;
        $this->factory = $factory;
        $this->aggregateType = $aggregateType;
    }

    public function load(string $aggregateId): AggregateRoot
    {
        $aggregate = $this->factory->build($this->aggregateType);
        $load = \Closure::bind(function($aggregate, $event) {
            $aggregate->events->inject($event);
        }, null, AnnotatedAggregateRoot::class);
        foreach ($this->store->load($this->aggregateType, $aggregateId) as $event) {
            $load($aggregate, $event);
        }
        return $aggregate;
    }

    public function save(string $aggregateId, AggregateRoot $aggregate)
    {
        $save = \Closure::bind(function($aggregate) {
            return $aggregate->events->extract();
        }, null, AnnotatedAggregateRoot::class);
        foreach ($save($aggregate) as $event) {
            $this->store->append($this->aggregateType, $aggregateId, $event);
        }
    }
}

