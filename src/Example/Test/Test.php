<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Test;

use Spray\Event\Annotation\AggregateId;
use Spray\Event\Annotation\AggregateRoot;
use Spray\Event\Annotation\CommandHandler;
use Spray\Event\Annotation\EventSourcingHandler;
use Spray\Event\Example\Test\Command\CommandWithNoTargetAggregateId;
use Spray\Event\Example\Test\Command\FirstCommandNoAggregateId;
use Spray\Event\Example\Test\Event\NoAggregateIdEvent;
use Spray\Event\Domain\AnnotatedAggregateRoot;

/**
 * @AggregateRoot
 */
class Test extends AnnotatedAggregateRoot
{
    /**
     * @AggregateId
     * @var TestId
     */
    private $testId;

    /**
     * @CommandHandler
     *
     * @param CommandWithNoTargetAggregateId $noAggregateId
     */
    public function noTarget(CommandWithNoTargetAggregateId $command)
    {

    }

    /**
     * @CommandHandler
     *
     * @param FirstCommandNoAggregateId $command
     */
    public function firstWithNoId(FirstCommandNoAggregateId $command)
    {
        $this->apply(new NoAggregateIdEvent());
    }

    /**
     * @EventSourcingHandler
     *
     * @param NoAggregateIdEvent $event
     */
    protected function noAggregateId(NoAggregateIdEvent $event)
    {

    }
}
