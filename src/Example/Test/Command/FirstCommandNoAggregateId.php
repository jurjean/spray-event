<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Test\Command;

use Spray\Event\Annotation as Event;
use Spray\Event\Example\Test\TestId;

class FirstCommandNoAggregateId
{
    /**
     * @var TestId
     */
    private $testId;

    /**
     * @param TestId $testId
     */
    public function __construct(TestId $testId)
    {
        $this->testId = $testId;
    }

    /**
     * @return TestId
     */
    public function getTestId()
    {
        return $this->testId;
    }
}