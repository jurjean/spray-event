<?php

namespace Spray\Event\Example;

use Assert\Assertion;

abstract class Identifier
{
    /**
     * @var string
     * @Event\Id
     */
    private $id;

    /**
     * @param string $id
     */
    public function __construct($id)
    {
        $this->id = (string) $id;
        Identifier::assertValid($this->id);
    }

    /**
     * @param $id
     */
    public static function assertValid($id)
    {
        Assertion::uuid($id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}
