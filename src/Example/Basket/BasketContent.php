<?php

declare(strict_types=1);

namespace Spray\Event\Example\Basket;

class BasketContent
{
    private $products = array();

    public function __construct(array $products = array())
    {
        $this->products = $products;
    }

    public function add(string $productId): BasketContent
    {
        $products = $this->products;
        if ( ! array_key_exists($productId, $products)) {
            $products[$productId] = 0;
        }
        $products[$productId]++;
        return new BasketContent($products);
    }

    public function remove(string $productId): BasketContent
    {
        $products = $this->products;
        $products[$productId]--;
        if ( ! array_key_exists($productId, $products)) {
            unset($products[$productId]);
        }
        return new BasketContent($products);
    }

    public function contains(string $productId)
    {
        return isset($this->products[$productId]);
    }
}