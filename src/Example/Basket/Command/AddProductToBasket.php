<?php

declare(strict_types=1);

namespace Spray\Event\Example\Basket\Command;

use Spray\Event\Example\Basket\BasketId;
use Spray\Event\Example\Basket\ProductId;

class AddProductToBasket
{
    /**
     * @var BasketId
     */
    private $basketId;

    /**
     * @var ProductId
     */
    private $productId;

    /**
     * @param BasketId $basketId
     * @param ProductId $productId
     */
    public function __construct(BasketId $basketId, ProductId $productId)
    {
        $this->basketId = $basketId;
        $this->productId = $productId;
    }

    /**
     * @return BasketId
     */
    public function getBasketId(): BasketId
    {
        return $this->basketId;
    }

    /**
     * @return string
     */
    public function getProductId(): ProductId
    {
        return $this->productId;
    }
}