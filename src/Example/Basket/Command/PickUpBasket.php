<?php

namespace Spray\Event\Example\Basket\Command;

use Spray\Event\Example\Basket\BasketId;

class PickUpBasket
{
    /**
     * @var BasketId
     */
    private $basketId;

    /**
     * @param BasketId $basketId
     */
    public function __construct(BasketId $basketId)
    {
        $this->basketId = $basketId;
    }

    /**
     * @return BasketId
     */
    public function getBasketId(): BasketId
    {
        return $this->basketId;
    }
}
