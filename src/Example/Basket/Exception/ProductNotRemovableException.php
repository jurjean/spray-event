<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Basket\Exception;

class ProductNotRemovableException extends DomainException
{

}