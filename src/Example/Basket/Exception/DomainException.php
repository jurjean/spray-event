<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Basket\Exception;

use RuntimeException;

class DomainException extends RuntimeException
{

}