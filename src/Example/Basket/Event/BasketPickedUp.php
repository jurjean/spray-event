<?php

namespace Spray\Event\Example\Basket\Event;

use Spray\Event\Example\Basket\BasketId;

class BasketPickedUp
{
    /**
     * @var BasketId
     */
    private $basketId;

    /**
     * @param BasketId $basketId
     */
    public function __construct(BasketId $basketId)
    {
        $this->basketId = $basketId;
    }

    /**
     * @return BasketId
     */
    public function getBasketId()
    {
        return $this->basketId;
    }
}