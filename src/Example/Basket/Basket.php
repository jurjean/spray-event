<?php

namespace Spray\Event\Example\Basket;

use RuntimeException;
use Spray\Event\Annotation\AggregateId;
use Spray\Event\Annotation\AggregateRoot;
use Spray\Event\Annotation\CommandHandler;
use Spray\Event\Annotation\EventSourcingHandler;
use Spray\Event\Example\Basket\Command\AddProductToBasket;
use Spray\Event\Example\Basket\Command\PickUpBasket;
use Spray\Event\Example\Basket\Command\RemoveProductFromBasket;
use Spray\Event\Example\Basket\Event\BasketPickedUp;
use Spray\Event\Example\Basket\Event\ProductAddedToBasket;
use Spray\Event\Example\Basket\Event\ProductRemovedFromBasket;
use Spray\Event\Example\Basket\Exception\ProductNotRemovableException;
use Spray\Event\Domain\AnnotatedAggregateRoot;

/**
 * @AggregateRoot
 */
class Basket extends AnnotatedAggregateRoot
{

    /**
     * @AggregateId
     *
     * @var BasketId
     */
    private $basketId;

    /**
     * @var BasketContent
     */
    private $contents;

    /**
     * @CommandHandler
     *
     * @param PickUpBasket $command
     *
     * @return void
     */
    public function pickUp(PickUpBasket $command)
    {
        $this->apply(new BasketPickedUp(
            $command->getBasketId()
        ));
    }

    /**
     * @CommandHandler
     *
     * @param AddProductToBasket $command
     *
     * @return void
     */
    public function addProduct(AddProductToBasket $command)
    {
        Basket::assertAddProduct($this, $command->getProductId());

        $this->apply(new ProductAddedToBasket(
            $command->getBasketId(),
            $command->getProductId()
        ));
    }

    /**
     * @CommandHandler
     *
     * @param RemoveProductFromBasket $command
     *
     * @return void
     */
    public function removeProduct(RemoveProductFromBasket $command)
    {
        Basket::assertRemoveProduct($this, $command->getProductId());

        $this->apply(new ProductRemovedFromBasket(
            $command->getBasketId(),
            $command->getProductId()
        ));
    }

    /**
     * @EventSourcingHandler
     *
     * @param BasketPickedUp $event
     */
    protected function pickedUp(BasketPickedUp $event)
    {
        $this->basketId = new BasketId($event->getBasketId());
        $this->contents = new BasketContent();
    }

    /**
     * @EventSourcingHandler
     *
     * @param ProductAddedToBasket $event
     */
    protected function productAdded(ProductAddedToBasket $event)
    {
        $this->contents = $this->contents->add($event->getProductId());
    }

    /**
     * @EventSourcingHandler
     *
     * @param ProductRemovedFromBasket $event
     *
     * @return void
     */
    protected function productRemoved(ProductRemovedFromBasket $event)
    {
        $this->contents = $this->contents->remove($event->getProductId());
    }

    /**
     * Assert a product can be added to the basket.
     *
     * @param Basket $basket
     * @param string $productId
     *
     * @return void
     *
     * @throws RuntimeException
     */
    private static function assertAddProduct(Basket $basket, string $productId)
    {

    }

    /**
     * Assert a product can be removed from the basket.
     *
     * @param Basket $basket
     * @param string $productId
     *
     * @return void
     *
     * @throws RuntimeException
     */
    private static function assertRemoveProduct(Basket $basket, string $productId)
    {
        if ( ! $basket->contents->contains($productId)) {
            throw new ProductNotRemovableException(sprintf('Product %s is not in the basket', $productId));
        }
    }
}
