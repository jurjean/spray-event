<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory;

use Spray\Event\Example\Identifier;

class ProductId extends Identifier
{

}