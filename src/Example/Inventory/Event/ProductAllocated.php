<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory\Event;

class ProductAllocated
{
    /**
     * @var string
     */
    private $inventoryId;

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $productId;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @param string $inventoryId
     * @param string $orderId
     * @param string $productId
     * @param int $quantity
     */
    public function __construct(string $inventoryId, string $orderId, string $productId, int $quantity)
    {
        $this->inventoryId = $inventoryId;
        $this->orderId = $orderId;
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getInventoryId()
    {
        return $this->inventoryId;
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
