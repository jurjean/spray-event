<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory\Command;

class ProductResupplied
{
    /**
     * @var string
     */
    private $inventoryId;

    /**
     * @var string
     */
    private $productId;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @param string $inventoryId
     * @param string $productId
     * @param int $quantity
     */
    public function __construct(string $inventoryId, string $productId, int $quantity)
    {
        $this->inventoryId = $inventoryId;
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getInventoryId()
    {
        return $this->inventoryId;
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}