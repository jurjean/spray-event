<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory\Command;

class InitiateProduct
{
    /**
     * @var string
     */
    private $inventoryId;

    /**
     * @var string
     */
    private $productId;

    /**
     * @param string $inventoryId
     * @param string $productId
     */
    public function __construct(string $inventoryId, string $productId)
    {
        $this->inventoryId = $inventoryId;
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getInventoryId()
    {
        return $this->inventoryId;
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }
}