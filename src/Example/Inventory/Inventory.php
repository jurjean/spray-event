<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory;

use Assert\Assertion;
use Spray\Event\Annotation\AggregateId;
use Spray\Event\Annotation\AggregateRoot;
use Spray\Event\Annotation\CommandHandler;
use Spray\Event\Annotation\EventSourcingHandler;
use Spray\Event\Example\Inventory\Command\AllocateProduct;
use Spray\Event\Example\Inventory\Command\InitiateProduct;
use Spray\Event\Example\Inventory\Event\ProductAllocated;
use Spray\Event\Example\Inventory\Event\ProductInitiated;
use Spray\Event\Domain\AnnotatedAggregateRoot;

/**
 * @AggregateRoot
 */
class Inventory extends AnnotatedAggregateRoot
{
    /**
     * @AggregateId
     * @var InventoryId
     */
    private $inventoryId;

    /**
     * @var Stock
     */
    private $stock;

    /**
     * @CommandHandler
     * @param InitiateProduct $command
     */
    public function initiate(InitiateProduct $command)
    {
        Inventory::assertInitiatable($this, $command);

        $this->apply(new ProductInitiated(
            $command->getInventoryId(),
            $command->getProductId()
        ));
    }

    /**
     * @CommandHandler
     * @param AllocateProduct $command
     */
    public function allocate(AllocateProduct $command)
    {
        Inventory::assertAllocatable($this, $command);

        $this->apply(new ProductAllocated(
            $command->getInventoryId(),
            $command->getOrderId(),
            $command->getProductId(),
            $command->getQuantity()
        ));
    }

    /**
     * @EventSourcingHandler
     * @param ProductInitiated $event
     */
    protected function initiated(ProductInitiated $event)
    {
        $this->inventoryId = new InventoryId($event->getInventoryId());
        $this->stock = new Stock(0);
    }

    /**
     * @EventSourcingHandler
     * @param ProductAllocated $event
     */
    protected function allocated(ProductAllocated $event)
    {
        $this->stock = $this->stock->subtract($event->getQuantity());
    }

    /**
     * @param Inventory $inventory
     * @param InitiateProduct $command
     */
    private static function assertInitiatable(Inventory $inventory, InitiateProduct $command)
    {
        Assertion::eq(null, $inventory->inventoryId);
        InventoryId::assertValid($command->getInventoryId());
        ProductId::assertValid($command->getProductId());
    }

    /**
     * @param Inventory $this
     * @param AllocateProduct $command
     */
    private static function assertAllocatable(Inventory $inventory, AllocateProduct $command)
    {
        InventoryId::assertValid($command->getInventoryId());
        Stock::assertSubtractable($inventory->stock, $command->getQuantity());
    }
}
