<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory;

final class Stock
{
    private $quantity;

    public function __construct(int $quantity)
    {
        $this->quantity = $quantity;
    }

    public function add(int $quantity): Stock
    {
        return new Stock($this->quantity, $quantity);
    }

    public function subtract(int $quantity): Stock
    {
        return new Stock($this->quantity - $quantity);
    }

    public static function assertSubtractable(Stock $stock, int $quantity)
    {

    }

    public static function assertAddable(Stock $stock, int $quantity)
    {

    }
}