<?php

declare(strict_types = 1);

namespace Spray\Event\Exception;

class RuntimeException extends \RuntimeException implements Exception
{

}