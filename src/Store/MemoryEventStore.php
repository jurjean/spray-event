<?php

declare(strict_types = 1);

namespace Spray\Event\Store;

use Generator;
use Spray\Event\Domain\Exception\AggregateNotFoundException;

class MemoryEventStore implements Store
{
    private $events = array();

    public function load(string $aggregateType, string $aggregateId): Generator
    {
        if ( ! isset($this->events[$aggregateType])) {
            $this->events[$aggregateType] = array();
        }
        if ( ! isset($this->events[$aggregateType][$aggregateId])) {
            $this->events[$aggregateType][$aggregateId] = array();
        }
        foreach($this->events[$aggregateType][$aggregateId] as $event) {
            yield $event;
        }
    }

    public function append(string $aggregateType, string $aggregateId, $event)
    {
        if ( ! isset($this->events[$aggregateType])) {
            $this->events[$aggregateType] = array();
        }
        if ( ! isset($this->events[$aggregateType][$aggregateId])) {
            $this->events[$aggregateType][$aggregateId] = array();
        }
        $this->events[$aggregateType][$aggregateId][] = $event;
    }
}