<?php

declare(strict_types = 1);

namespace Spray\Event\Store;

use Generator;

class RecordingEventStore implements Store
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var boolean
     */
    private $recording;

    /**
     * @var array
     */
    private $recorded = array();

    /**
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }
    /**
     * Start recording.
     *
     * @return void
     */
    public function start()
    {
        $this->recording = true;
    }

    /**
     * Stop recording.
     *
     * @return void
     */
    public function stop(): array
    {
        $this->recording = false;
        $recorded = $this->recorded;
        $this->recorded = array();
        return $recorded;
    }

    public function load(string $aggregateType, string $aggregateId): Generator
    {
        return $this->store->load($aggregateType, $aggregateId);
    }

    public function append(string $aggregateType, string $aggregateId, $event)
    {
        $this->store->append($aggregateType, $aggregateId, $event);
        if ($this->recording) {
            $this->recorded[] = $event;
        }
    }
}