<?php

declare(strict_types=1);

namespace Spray\Event\Store;

use Generator;
use Spray\Event\Domain\AggregateRoot;

interface Store
{
    public function load(string $aggregateType, string $aggregateId): Generator;

    public function append(string $aggregateType, string $aggregateId, $events);
}
