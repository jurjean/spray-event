<?php

declare(strict_types = 1);

namespace Spray\Event\CommandHandling;

use Doctrine\Common\Annotations\AnnotationReader;
use Spray\Event\Annotation;
use ReflectionClass;
use Spray\Event\CommandHandling\Exception\UnknownAggregateException;
use Spray\Event\CommandHandling\Exception\UnknownCommandException;
use Spray\Event\Domain\Repository;

class AnnotatedAggregateCommandHandler implements CommandHandler
{
    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var array
     */
    private $handlers = array();

    public function __construct(
        AnnotationReader $annotationReader,
        Repository $repository,
        string $aggregateType)
    {
        $this->annotationReader = $annotationReader;
        $this->repository = $repository;
        $reflection = new ReflectionClass($aggregateType);
        foreach ($reflection->getProperties() as $propertyReflection) {
            $annotations = $annotationReader->getPropertyAnnotations($propertyReflection);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Annotation\AggregateId) {
                    $this->aggregateId = $propertyReflection->getName();
                }
            }
        }
        foreach ($reflection->getMethods() as $methodReflection) {
            $annotations = $annotationReader->getMethodAnnotations($methodReflection);
            foreach ($annotations as $annotation) {
                if ($annotation instanceof Annotation\CommandHandler) {
                    $methodName = $methodReflection->getName();
                    $this->handlers[$methodReflection->getParameters()[0]->getClass()->getName()] = function($command) use ($methodName) {
                        $this->$methodName($command);
                    };
                }
            }
        }
    }

    public static function factory(string $aggregateType, Repository $aggregateRepository)
    {
        return new AnnotatedAggregateCommandHandler(
            new AnnotationReader(),
            $aggregateRepository,
            $aggregateType
        );
    }

    private function aggregateTargetId($command)
    {
        $method = 'get' . ucfirst($this->aggregateId);
        if ( ! method_exists($command, $method)) {
            throw new UnknownAggregateException(sprintf(
                'Could not determine target aggregate id for command %s. Please create method %s()',
                get_class($command),
                $method
            ));
        }
        return (string) $command->$method();
    }

    public function accepts($command)
    {
        return isset($this->handlers[get_class($command)]);
    }

    public function handle($command)
    {
        if ( ! $this->accepts($command)) {
            throw new UnknownCommandException(sprintf(
                'Command %s cannot be handled by this handler',
                get_class($command)
            ));
        }

        $aggregate = $this->repository->load($this->aggregateTargetId($command));

        $this->handlers[get_class($command)]->call($aggregate, $command);

        $this->repository->save($this->aggregateTargetId($command), $aggregate);
    }
}
