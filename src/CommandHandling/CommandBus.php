<?php

declare(strict_types = 1);

namespace Spray\Event\CommandHandling;

interface CommandBus
{
    public function register(CommandHandler $commandHandler);

    public function dispatch($command);
}
