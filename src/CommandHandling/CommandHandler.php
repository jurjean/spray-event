<?php

declare(strict_types = 1);

namespace Spray\Event\CommandHandling;

interface CommandHandler
{
    public function accepts($command);

    public function handle($command);
}
