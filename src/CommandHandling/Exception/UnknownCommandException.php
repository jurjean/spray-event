<?php

declare(strict_types = 1);

namespace Spray\Event\CommandHandling\Exception;

use Spray\Event\Exception\Exception;
use Spray\Event\Exception\RuntimeException;

class UnknownCommandException extends RuntimeException implements Exception
{

}