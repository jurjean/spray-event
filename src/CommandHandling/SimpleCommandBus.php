<?php

declare(strict_types = 1);

namespace Spray\Event\CommandHandling;

use RuntimeException;
use Spray\Event\CommandHandling\Exception\UnknownCommandException;

class SimpleCommandBus implements CommandBus
{
    /**
     * @var array<CommandHandler>
     */
    private $handlers = array();

    public function register(CommandHandler $commandHandler)
    {
        $this->handlers[] = $commandHandler;
    }

    public function dispatch($command)
    {
        /**
         * @var CommandHandler $handler
         */
        foreach ($this->handlers as $handler) {
            if ( ! $handler->accepts($command)) {
                continue;
            }

            $handler->handle($command);
            return;
        }

        throw new UnknownCommandException(sprintf(
            'Command %s not handled by any command handler',
            get_class($command)
        ));
    }
}