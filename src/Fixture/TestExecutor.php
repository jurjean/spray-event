<?php

declare(strict_types=1);

namespace Spray\Event\Fixture;

interface TestExecutor
{
    /**
     * @param object $command
     *
     * @return ResultValidator
     */
    public function when($command): ResultValidator;
}
