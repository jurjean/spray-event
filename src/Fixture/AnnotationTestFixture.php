<?php

declare(strict_types = 1);

namespace Spray\Event\Fixture;

use Exception;
use PHPUnit_Framework_TestCase as TestCase;
use Spray\Event\CommandHandling\CommandBus;
use Spray\Event\CommandHandling\CommandHandler;
use Spray\Event\CommandHandling\SimpleCommandBus;
use Spray\Event\Exception\Exception as SprayEventException;
use Spray\Event\Domain\AggregateFactory;
use Spray\Event\Domain\AggregateRepository;
use Spray\Event\Domain\AnnotatedAggregateFactory;
use Spray\Event\Store\MemoryEventStore;
use Spray\Event\Store\RecordingEventStore;

class AnnotationTestFixture implements TestFixture, TestExecutor
{
    /**
     * @var TestCase
     */
    private $testCase;

    /**
     * @var string
     */
    private $aggregateType;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var RecordingEventStore
     */
    private $eventStore;

    /**
     * @var AggregateFactory
     */
    private $aggregateFactory;

    /**
     * @param TestCase $testCase
     * @param CommandHandler ...$commandHandlers
     */
    public function __construct(TestCase $testCase, string $aggregateType, CommandHandler ...$commandHandlers)
    {
        $this->testCase = $testCase;
        $this->aggregateType = $aggregateType;

        foreach ($commandHandlers as $commandHandler) {
            $this->registerCommandHandler($commandHandler);
        }
    }

    /**
     * @param CommandHandler $commandHandler
     */
    public function registerCommandHandler(CommandHandler $commandHandler)
    {
        $this->commandBus()->register($commandHandler);
    }

    /**
     * @param string $aggregateType
     */
    public function createRepository(string $aggregateType)
    {
        return new AggregateRepository($this->eventStore(), $this->aggregateFactory(), $aggregateType);
    }

    /**
     * @return CommandBus
     */
    public function commandBus()
    {
        if (null === $this->commandBus) {
            $this->commandBus = new SimpleCommandBus();
        }
        return $this->commandBus;
    }

    /**
     * @return RecordingEventStore
     */
    public function eventStore()
    {
        if (null === $this->eventStore) {
            $this->eventStore = new RecordingEventStore(new MemoryEventStore());
        }
        return $this->eventStore;
    }

    /**
     * @return AggregateFactory
     */
    public function aggregateFactory()
    {
        if (null === $this->aggregateFactory) {
            $this->aggregateFactory = new AnnotatedAggregateFactory();
        }
        return $this->aggregateFactory;
    }

    public function withAggregateId($aggregateId): TestFixture
    {
        $this->aggregateId = (string) $aggregateId;
        return $this;
    }

    public function givenNoActivity(): TestExecutor
    {
        return $this;
    }

    public function givenCommands(...$commands): TestExecutor
    {
        foreach ($commands as $command) {
            $this->commandBus()->dispatch($command);
        }
        return $this;
    }

    public function givenEvents(...$events): TestExecutor
    {
        if (null === $this->aggregateId) {
            throw new \UnexpectedValueException(sprintf(
                'You need to provide an aggregate id to give events. Call withAggregateId() first'
            ));
        }
        foreach ($events as $event) {
            $this->eventStore()->append($this->aggregateType, $this->aggregateId, $event);
        }
        return $this;
    }

    /**
     * @param object $command
     *
     * @return ResultValidator
     */
    public function when($command): ResultValidator
    {
        $this->eventStore()->start();
        try {
            $this->commandBus()->dispatch($command);
        } catch (SprayEventException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            return new AnnotationResultValidator($this->testCase, array_merge(array($exception), $this->eventStore()->stop()));
        }
        return new AnnotationResultValidator($this->testCase, $this->eventStore()->stop());
    }
}
