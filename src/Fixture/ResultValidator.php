<?php

declare(strict_types=1);

namespace Spray\Event\Fixture;

use Exception;

interface ResultValidator
{
    public function expectNoEvents(): ResultValidator;

    public function expectEventCount(int $count): ResultValidator;

    public function expectEvents(...$events): ResultValidator;

    public function expectEventsMatching(...$events): ResultValidator;

    public function expectNoException(): ResultValidator;

    public function expectExceptionType(string $exceptionType): ResultValidator;
}
