<?php

declare(strict_types = 1);

namespace Spray\Event\Fixture;

use Exception;
use PHPUnit_Framework_TestCase as TestCase;

class AnnotationResultValidator implements ResultValidator
{
    /**
     * @var TestCase
     */
    private $testCase;

    /**
     * @var null|Exception
     */
    private $exception;

    /**
     * @var array<object>
     */
    private $events = array();

    /**
     * @param TestCase $testCase
     * @param ...$results
     */
    public function __construct(TestCase $testCase, $results)
    {
        $this->testCase = $testCase;
        foreach ($results as $result) {
            if ($result instanceof Exception) {
                $this->exception = $result;
            } else {
                $this->events[] = $result;
            }
        }
    }

    public function expectNoEvents(): ResultValidator
    {
        return $this;
    }

    public function expectEventCount(int $count): ResultValidator
    {
        $this->testCase->assertCount(
            $count,
            $this->events,
            sprintf('Expected event count of %s but was %s', $count, count($this->events))
        );
        return $this;
    }

    public function expectEvents(...$events): ResultValidator
    {
        $this->testCase->assertEquals($events, $this->events, 'Expected event streams do not match');
        return $this;
    }

    public function expectEventsMatching(...$events): ResultValidator
    {
        $this->testCase->assertFalse(true, 'Event matching not yet implemented');
        return $this;
    }

    public function expectNoException(): ResultValidator
    {
        $this->testCase->assertNull(
            $this->exception,
            sprintf('No exception was expected but %s was thrown', get_class($this->exception))
        );
        return $this;
    }

    public function expectExceptionType(string $exceptionType): ResultValidator
    {
        $this->testCase->assertNotNull($this->exception, 'An exception was expected but none was thrown');
        $this->testCase->assertInstanceOf(
            $exceptionType,
            $this->exception,
            sprintf('Expected exception %s but %s was thrown: %s', $exceptionType, get_class($this->exception), $this->exception->getMessage())
        );
        return $this;
    }
}