<?php

declare(strict_types=1);

namespace Spray\Event\Fixture;

interface TestFixture
{
    public function withAggregateId($aggregateId): TestFixture;

    /**
     * @return TestExecutor
     */
    public function givenNoActivity(): TestExecutor;

    /**
     * @return TestExecutor
     */
    public function givenCommands(...$commands): TestExecutor;

    /**
     * @return TestExecutor
     */
    public function givenEvents(...$events): TestExecutor;
}
