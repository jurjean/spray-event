<?php

namespace Spray\Event\Example\Basket;

use Doctrine\Common\Annotations\AnnotationRegistry;
use PHPUnit_Framework_TestCase;
use Ramsey\Uuid\Uuid;
use Spray\Event\Annotation\AnnotationLoader;
use Spray\Event\CommandHandling\AnnotatedAggregateCommandHandler;
use Spray\Event\Example\Basket\Command\AddProductToBasket;
use Spray\Event\Example\Basket\Command\PickUpBasket;
use Spray\Event\Example\Basket\Command\RemoveProductFromBasket;
use Spray\Event\Example\Basket\Event\BasketPickedUp;
use Spray\Event\Example\Basket\Event\ProductAddedToBasket;
use Spray\Event\Example\Basket\Event\ProductRemovedFromBasket;
use Spray\Event\Example\Basket\Exception\ProductNotRemovableException;
use Spray\Event\Fixture\AnnotationTestFixture;
use Spray\Event\Domain\AggregateRepository;

class BasketTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var AnnotationTestFixture
     */
    private $fixture;

    protected function setUp()
    {
        AnnotationRegistry::reset();
        AnnotationRegistry::registerLoader(new AnnotationLoader());

        $this->fixture = new AnnotationTestFixture($this, Basket::class);
        $this->fixture->registerCommandHandler(AnnotatedAggregateCommandHandler::factory(
            Basket::class,
            $this->fixture->createRepository(Basket::class)
        ));
    }

    public function testPickUpBasket()
    {
        $basketId = new BasketId(Uuid::uuid4());

        $this->fixture->givenNoActivity()
            ->when(new PickUpBasket($basketId))
            ->expectEvents(new BasketPickedUp($basketId));
    }

    public function testAddProductToBasket()
    {
        $basketId = new BasketId(Uuid::uuid4());
        $productId = new ProductId(Uuid::uuid4());

        $this->fixture->withAggregateId($basketId)
            ->givenEvents(new BasketPickedUp($basketId))
            ->when(new AddProductToBasket($basketId, $productId))
            ->expectEvents(new ProductAddedToBasket($basketId, $productId));
    }

    public function testCannotRemovedProductIfNotAdded()
    {
        $basketId = new BasketId(Uuid::uuid4());
        $productId = new ProductId(Uuid::uuid4());

        $this->fixture->withAggregateId($basketId)
            ->givenEvents(
                new BasketPickedUp($basketId))
            ->when(new RemoveProductFromBasket($basketId, $productId))
            ->expectExceptionType(ProductNotRemovableException::class);
    }

    public function testRemoveAddedProductToBasket()
    {
        $basketId = new BasketId(Uuid::uuid4());
        $productId = new ProductId(Uuid::uuid4());

        $this->fixture->withAggregateId($basketId)
            ->givenEvents(
                new BasketPickedUp($basketId),
                new ProductAddedToBasket($basketId, $productId))
            ->when(new RemoveProductFromBasket($basketId, $productId))
            ->expectEvents(
                new ProductRemovedFromBasket($basketId, $productId)
            );
    }

    public function testRemoveAddedTwiceProductToBasket()
    {
        $basketId = new BasketId(Uuid::uuid4());
        $productId = new ProductId(Uuid::uuid4());

        $this->fixture->withAggregateId($basketId)
            ->givenEvents(
                new BasketPickedUp($basketId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductAddedToBasket($basketId, $productId),
                new ProductRemovedFromBasket($basketId, $productId))
            ->when(new RemoveProductFromBasket($basketId, $productId))
            ->expectEvents(
                new ProductRemovedFromBasket($basketId, $productId)
            );
    }
}
