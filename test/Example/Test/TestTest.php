<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Test;

use PHPUnit_Framework_TestCase;
use Ramsey\Uuid\Uuid;
use Spray\Event\CommandHandling\AnnotatedAggregateCommandHandler;
use Spray\Event\CommandHandling\Exception\UnknownAggregateException;
use Spray\Event\CommandHandling\Exception\UnknownCommandException;
use Spray\Event\EventHandling\Exception\AggregateNotIdentifiedException;
use Spray\Event\Example\Test\Command\CommandWithNoTargetAggregateId;
use Spray\Event\Example\Test\Command\FirstCommandNoAggregateId;
use Spray\Event\Example\Test\Command\UnknownCommand;
use Spray\Event\Fixture\AnnotationTestFixture;
use Spray\Event\Fixture\TestFixture;

class TestTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TestFixture
     */
    private $fixture;

    protected function setUp()
    {
        $this->fixture = new AnnotationTestFixture($this, Test::class);
        $this->fixture->registerCommandHandler(AnnotatedAggregateCommandHandler::factory(
            Test::class,
            $this->fixture->createRepository(Test::class)
        ));
    }

    public function testUnknownCommand()
    {
        $this->setExpectedException(UnknownCommandException::class);
        $this->fixture->givenNoActivity()
            ->when(new UnknownCommand())
            ->expectNoEvents();
    }

    public function testCreateNoTargetAggregateId()
    {
        $this->setExpectedException(UnknownAggregateException::class);
        $this->fixture->givenNoActivity()
            ->when(new CommandWithNoTargetAggregateId())
            ->expectNoEvents();
    }

    public function testCreateNoAggregateId()
    {
        $testId = new TestId(Uuid::uuid4());

        $this->setExpectedException(AggregateNotIdentifiedException::class);
        $this->fixture->givenNoActivity()
            ->when(new FirstCommandNoAggregateId($testId))
            ->expectNoEvents();
    }
}
