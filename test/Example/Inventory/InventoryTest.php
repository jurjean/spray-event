<?php

declare(strict_types = 1);

namespace Spray\Event\Example\Inventory;

use PHPUnit_Framework_TestCase;
use Ramsey\Uuid\Uuid;
use Spray\Event\CommandHandling\AnnotatedAggregateCommandHandler;
use Spray\Event\Example\Inventory\Command\AllocateProduct;
use Spray\Event\Example\Inventory\Command\InitiateProduct;
use Spray\Event\Example\Inventory\Event\ProductAllocated;
use Spray\Event\Example\Inventory\Event\ProductInitiated;
use Spray\Event\Fixture\AnnotationTestFixture;
use Spray\Event\Fixture\TestFixture;

class InventoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var TestFixture
     */
    private $fixture;

    protected function setUp()
    {
        $this->fixture = new AnnotationTestFixture($this, Inventory::class);
        $this->fixture->registerCommandHandler(AnnotatedAggregateCommandHandler::factory(
            Inventory::class,
            $this->fixture->createRepository(Inventory::class)
        ));
    }

    public function testInitiateInventory()
    {
        $inventoryId = (string) Uuid::uuid4();
        $productId = (string) Uuid::uuid4();

        $this->fixture->givenNoActivity()
            ->when(new InitiateProduct($inventoryId, $productId))
            ->expectEvents(new ProductInitiated($inventoryId, $productId));
    }

    public function testAllocateProduct()
    {
        $inventoryId = (string) Uuid::uuid4();
        $orderId = (string) Uuid::uuid4();
        $productId = (string) Uuid::uuid4();
        $quantity = 1;

        $this->fixture->givenCommands(new InitiateProduct($inventoryId, $productId))
            ->when(new AllocateProduct($inventoryId, $orderId, $productId, $quantity))
            ->expectEvents(new ProductAllocated($inventoryId, $orderId, $productId, $quantity));
    }
}
