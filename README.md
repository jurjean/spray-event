Spray\Event
===========

PHP event sourcing library, inspired by (but not limited to) the Axon Framework.

Developing
----------

You can unittest using the provided docker box. You'll need both docker and docker-compose.
To test the project, first build the container.

    docker-compose build

Then, run the unittest.

   docker-compose run phpunit